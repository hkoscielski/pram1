package com.hkoscielski.pram1.model

import com.hkoscielski.pram1.model.CustomImage

object CustomImages {
    val images: List<CustomImage> = listOf(
            CustomImage("https://randomuser.me/api/portraits/men/1.jpg", "1"),
            CustomImage("https://randomuser.me/api/portraits/men/2.jpg", "2"),
            CustomImage("https://randomuser.me/api/portraits/men/3.jpg", "3"),
            CustomImage("https://randomuser.me/api/portraits/men/4.jpg", "4"),
            CustomImage("https://randomuser.me/api/portraits/men/5.jpg", "5"),
            CustomImage("https://randomuser.me/api/portraits/men/6.jpg", "6"),
            CustomImage("https://randomuser.me/api/portraits/men/7.jpg", "7"),
            CustomImage("https://randomuser.me/api/portraits/men/8.jpg", "8"),
            CustomImage("https://randomuser.me/api/portraits/men/9.jpg", "9"),
            CustomImage("https://randomuser.me/api/portraits/men/10.jpg", "10"),
            CustomImage("https://randomuser.me/api/portraits/men/11.jpg", "11"),
            CustomImage("https://randomuser.me/api/portraits/men/12.jpg", "12")
    )
}