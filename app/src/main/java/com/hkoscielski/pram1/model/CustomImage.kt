package com.hkoscielski.pram1.model

import android.os.Parcel
import android.os.Parcelable

class CustomImage(val url: String, val title: String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(url)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CustomImage> {
        override fun createFromParcel(parcel: Parcel): CustomImage {
            return CustomImage(parcel)
        }

        override fun newArray(size: Int): Array<CustomImage?> {
            return arrayOfNulls(size)
        }
    }
}