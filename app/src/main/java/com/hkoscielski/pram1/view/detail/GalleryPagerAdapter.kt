package com.hkoscielski.pram1.view.detail

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.hkoscielski.pram1.model.CustomImage


class GalleryPagerAdapter(fm: FragmentManager,
                          private val images: List<CustomImage>) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val image = images[position]
        return ImageDetailFragment.newInstance(image)
    }

    override fun getCount(): Int {
        return images.size
    }
}