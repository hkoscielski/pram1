package com.hkoscielski.pram1.view.gallery

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.hkoscielski.pram1.GlideApp
import com.hkoscielski.pram1.R
import com.hkoscielski.pram1.model.CustomImage
import kotlinx.android.synthetic.main.custom_image_item.view.*

class GalleryAdapter(private val images: List<CustomImage>,
                     private val rowCount: Int? = null,
                     private val galleryItemClickListener: GalleryItemClickListener) : RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {

    private lateinit var recyclerView: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = LayoutInflater.from(parent.context).inflate(R.layout.custom_image_item, parent, false)
        rowCount?.let {
            viewHolder.layoutParams.height = recyclerView.measuredHeight / rowCount
        }
        return ViewHolder(viewHolder)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(images[position], galleryItemClickListener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivPhoto: ImageView = itemView.ivPhoto

        fun bind(image: CustomImage, galleryItemClickListener: GalleryItemClickListener) {
            GlideApp.with(itemView.context)
                    .load(image.url)
                    .placeholder(R.drawable.placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(ivPhoto)
            itemView.setOnClickListener { galleryItemClickListener.onItemClick(image, adapterPosition) }
        }
    }
}