package com.hkoscielski.pram1.view.detail

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hkoscielski.pram1.R
import com.hkoscielski.pram1.model.CustomImage
import kotlinx.android.synthetic.main.fragment_gallery_view_pager.*

class GalleryViewPagerFragment : Fragment() {

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity?.setTheme(R.style.AppTheme_Fullscreen)
    }

    override fun onDetach() {
        super.onDetach()
        activity?.setTheme(R.style.AppTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gallery_view_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val images: List<CustomImage> = arguments?.getParcelableArrayList(EXTRA_IMAGES)!!
        val currentItem: Int = arguments?.getInt(EXTRA_INITIAL_POSITION)!!

        galleryViewPager.adapter = GalleryPagerAdapter(childFragmentManager, images)
        galleryViewPager.currentItem = currentItem
    }

    companion object {
        private const val EXTRA_IMAGES = "GalleryViewPagerFragment.EXTRA_IMAGES"
        private const val EXTRA_INITIAL_POSITION = "GalleryViewPagerFragment.EXTRA_INITIAL_POSITION"

        fun newInstance(images: ArrayList<CustomImage>, position: Int): GalleryViewPagerFragment {
            val fragment = GalleryViewPagerFragment()
            val args = Bundle()
            args.putParcelableArrayList(EXTRA_IMAGES, images)
            args.putInt(EXTRA_INITIAL_POSITION, position)
            fragment.arguments = args
            return fragment
        }
    }
}