package com.hkoscielski.pram1.view.gallery

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hkoscielski.pram1.R
import com.hkoscielski.pram1.model.CustomImage
import com.hkoscielski.pram1.model.CustomImages
import com.hkoscielski.pram1.view.detail.GalleryViewPagerFragment
import kotlinx.android.synthetic.main.fragment_gallery.*

class GalleryFragment : Fragment(), GalleryItemClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvImages.layoutManager = GridLayoutManager(context, 2)
        rvImages.adapter = GalleryAdapter(CustomImages.images, 4, this)
        val snapHelper = StartSnapHelper()
        snapHelper.attachToRecyclerView(rvImages)
    }

    override fun onItemClick(image: CustomImage, position: Int) {
        val galleryViewPagerFragment = GalleryViewPagerFragment.newInstance(ArrayList(CustomImages.images), position)
        fragmentManager!!.beginTransaction()
                .addToBackStack(TAG)
                .replace(R.id.galleryContainer, galleryViewPagerFragment)
                .commit()
    }

    companion object {
        private val TAG = GalleryFragment::class.simpleName

        fun newInstance(): GalleryFragment {
            return GalleryFragment()
        }
    }
}
