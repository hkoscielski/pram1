package com.hkoscielski.pram1.view.gallery

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.hkoscielski.pram1.R

class GalleryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)

        supportFragmentManager.beginTransaction()
                .add(R.id.galleryContainer, GalleryFragment.newInstance())
                .commit()
    }
}
