package com.hkoscielski.pram1.view.detail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hkoscielski.pram1.GlideApp
import com.hkoscielski.pram1.R
import com.hkoscielski.pram1.model.CustomImage
import kotlinx.android.synthetic.main.fragment_image_detail.*

class ImageDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val image: CustomImage = arguments!!.getParcelable(EXTRA_CUSTOM_IMAGE)
        GlideApp.with(activity!!)
                .asBitmap()
                .load(image.url)
                .into(detailImage)
    }

    companion object {
        private const val EXTRA_CUSTOM_IMAGE = "ImageDetailFragment.EXTRA_CUSTOM_IMAGE"

        fun newInstance(image: CustomImage): ImageDetailFragment {
            val fragment = ImageDetailFragment()
            val args = Bundle()
            args.putParcelable(EXTRA_CUSTOM_IMAGE, image)
            fragment.arguments = args
            return fragment
        }
    }
}