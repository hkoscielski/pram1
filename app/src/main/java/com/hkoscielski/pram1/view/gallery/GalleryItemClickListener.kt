package com.hkoscielski.pram1.view.gallery

import com.hkoscielski.pram1.model.CustomImage

interface GalleryItemClickListener {

    fun onItemClick(image: CustomImage, position: Int)
}